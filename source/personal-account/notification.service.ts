export interface INotificationService<TIn = string> {
  sendWelcomeMessage(account: TIn)
}


export class NotificationService implements INotificationService<string> {
  sendWelcomeMessage(email: string): void {
    const subject = 'Welcome'
    const message = `<h1>Welcome again`
    return await emailAdapter.sendEmail(email, subject, message)
  }
}