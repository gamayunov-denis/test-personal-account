import {AccountStatus} from "./account";

export class GetAccountsQuery {
id?: string;
email?: string;
name?: string;
number?: string;
status?: AccountStatus;
}
