export class Account {
  public id: string;
  public email: string;
  public name: string;
  public number: string;
  public status: AccountStatus;
}

export enum AccountStatus  {
  OPEN= 'Open',
  CLOSE= 'Close'
}


