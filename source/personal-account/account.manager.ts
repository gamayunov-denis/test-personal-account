import {OpenAccountCommand} from "./open-account.command";
import {CloseAccountCommand} from "./close-account.command";
import {GetAccountsQuery} from "./get-accounts.query";
import {Account, AccountStatus} from "./account";
import {AccountError} from "./account.error";
import {v4 as uuidv4} from 'uuid';
import {NotificationService} from "./notification.service";

export interface IAccountManager {
  openAccount(command: OpenAccountCommand): Promise<Account>
  closeAccount(command: CloseAccountCommand): Promise<Account>
  getAccounts(query: GetAccountsQuery): Promise<Account[]>
}

export class AccountManager implements IAccountManager {
  constructor(private notificationService: NotificationService) {
  }

  async openAccount(command: OpenAccountCommand): Promise<Account> {
    if (!command.name || command.name.trim() === '') {
      throw new AccountError('Name cannot be empty');
    }

    if (!command.email || command.email.trim() === '') {
      throw new AccountError('Email address cannot be empty');
    }

    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!emailRegex.test(command.email)) {
      throw new AccountError('Email should be a valid email');
    }



    const newAccount: Account =   {
      id: uuidv4(),
      email: command.email,
      name: command.name,
      number: uuidv4(),
      status: AccountStatus.OPEN,
    };

    this.notificationService.sendWelcomeMessage(newAccount.email);

    return newAccount;

  }

  async closeAccount(command: CloseAccountCommand): Promise<Account> {
    const closedAccount: Account = await this.findAccountById(command.id);

    if (!closedAccount) {
      throw new AccountError('Account not found');
    }

    if (closedAccount.status === AccountStatus.CLOSE) {
      throw new AccountError('Account already closed');
    }

    closedAccount.status = AccountStatus.CLOSE;

    return closedAccount;
  }

  async getAccounts(query: GetAccountsQuery): Promise<Account[]> {
    return await this.findAccounts();
  }
}

